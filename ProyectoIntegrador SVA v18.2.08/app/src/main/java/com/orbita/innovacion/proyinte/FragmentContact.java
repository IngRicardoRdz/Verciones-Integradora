package com.orbita.innovacion.proyinte;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.spark.submitbutton.SubmitButton;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class FragmentContact extends Fragment {

    String correo = "yagari2017@gmail.com", contrasena = "YAGARIINC2017";
    String a = "null", b = "null";

    EditText name, address, mensaje;
    SubmitButton envar;
    Session session;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment_contact, container, false);

        a = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Nombre","");
        b = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Email","");

        name = (EditText) v.findViewById(R.id.edtname);
        address = (EditText) v.findViewById(R.id.edtcorreo);
        mensaje = (EditText) v.findViewById(R.id.edtmensaje);
        envar = (SubmitButton) v.findViewById(R.id.btmenviar);

        name.setText(a);
        address.setText(b);

        envar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                envar.setEnabled(false);

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                Properties properties = new Properties();
                properties.put("mail.smtp.host", "smtp.googlemail.com");
                properties.put("mail.smtp.socketFactory.port", "465");
                properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                properties.put("mail.smtp.auth", "true");
                properties.put("mail.smtp.port", "465");

                try {
                    session=Session.getDefaultInstance(properties, new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(correo,contrasena);
                        }
                    });

                    if(session != null){
                        javax.mail.Message message = new MimeMessage(session);
                        message.setFrom(new InternetAddress(correo));
                        message.setSubject(a + " " + b);
                        message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse("rodriguez231402@hotmail.com"));
                        message.setContent(mensaje.getText().toString(), "text/html; charset=utf-8");
                        Transport.send(message);
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                name.setText("");
                address.setText("");
                mensaje.setText("");

            }
        });

        return v;
    }

}
